import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./actions/userServer";

export const store = configureStore({
  reducer: {
    user: userReducer,
  },
});
