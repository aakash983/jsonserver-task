// export const URL = "http://localhost:3000/todoList";

// // CRUD
// export const getUrl = `${URL}`;

// api.js
import axios from "axios";

const API_URL = "http://localhost:3000/todoList";

export const getItems = () => axios.get(API_URL);
export const createItem = (data) => axios.post(API_URL, data);
export const updateItem = (id, data) => axios.put(`${API_URL}/${id}`, data);
