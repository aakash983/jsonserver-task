import React from "react";
import { useForm, Controller } from "react-hook-form";
import {
  TextField,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControl,
} from "@mui/material";

const ModalForm = ({ open, handleClose, onSubmit, defaultValues }) => {
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    defaultValues,
  });

  return (
    <Dialog open={open} onClose={handleClose}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>{defaultValues ? "Edit FAQ" : "Create FAQ"}</DialogTitle>
        <DialogContent>
          <FormControl fullWidth>
            <Controller
              name="title"
              control={control}
              rules={{ required: "Title is required" }}
              render={({ field }) => (
                <TextField
                  placeholder="Please enter title here..."
                  defaultValue={defaultValues ? defaultValues.title : ""}
                  {...field}
                  fullWidth
                  margin="normal"
                  error={Boolean(errors.title)}
                  helperText={errors.title?.message}
                />
              )}
            />
          </FormControl>
          <FormControl fullWidth>
            <Controller
              name="description"
              control={control}
              rules={{ required: "Description is required" }}
              render={({ field }) => (
                <TextField
                  placeholder="Please enter description here..."
                  defaultValue={defaultValues ? defaultValues.description : ""}
                  multiline
                  maxRows={4}
                  {...field}
                  fullWidth
                  margin="normal"
                  error={Boolean(errors.description)}
                  helperText={errors.description?.message}
                />
              )}
            />
          </FormControl>
        </DialogContent>
        <DialogActions sx={{ justifyContent: "center" }}>
          <Button
            onClick={handleClose}
            variant="outlined"
            sx={{
              color: "#212121",
              borderColor: "#212121",
              "&:hover": {
                borderColor: "#212121",
              },
            }}
          >
            Cancel
          </Button>
          <Button
            type="submit"
            variant="contained"
            sx={{
              color: "#212121",
              background: "#73CFF9",
              "&:hover": {
                backgroundColor: "#73CFF9",
              },
            }}
          >
            {defaultValues ? "Update" : "Save"}
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
export default ModalForm;
