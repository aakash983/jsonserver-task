import React from "react";
import { Dialog, DialogTitle, DialogActions, Button } from "@mui/material";
const AlertPopUp = ({ open, onClose, onSubmitClick, submitButton, title }) => {
  return (
    <Dialog maxWidth="sm" open={open} onClose={onClose}>
      <DialogTitle>{title}</DialogTitle>
      <DialogActions sx={{ justifyContent: "center" }}>
        <Button
          variant="outlined"
          onClick={onClose}
          sx={{
            color: "#212121",
            borderColor: "#212121",
            "&:hover": {
              borderColor: "#212121",
            },
          }}
        >
          Close
        </Button>
        <Button
          variant="contained"
          onClick={onSubmitClick}
          sx={{
            color: "#212121",
            background: "#73CFF9",
            "&:hover": {
              backgroundColor: "#73CFF9",
            },
          }}
        >
          {submitButton}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default AlertPopUp;
