import React, { useState, useEffect } from "react";
import {
  Button,
  ButtonGroup,
  Container,
  Box,
  Typography,
  ListItem,
  List,
  Collapse,
  IconButton,
} from "@mui/material";
import { getItems, createItem, updateItem } from "../Constants/Constant";
import ModalForm from "../Modal/ModalForm";
import { ExpandMore as ExpandMoreIcon } from "@mui/icons-material";
import { ExpandLess as ExpandLessIcon } from "@mui/icons-material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import AlertPopUp from "../Modal/AlertPopUp";

const Home = () => {
  const style = {
    py: 0,
    borderRadius: 2,
    border: "1px solid",
    borderColor: "divider",
    backgroundColor: "background.paper",
    margin: 1,
    width: "100%",
    maxWidth: "100%",
  };
  const [items, setItems] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState(null);

  const fetchData = async () => {
    const response = await getItems();
    setItems(response.data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleOpenModal = (item) => {
    setSelectedItem(item);
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setSelectedItem(null);
    setOpenModal(false);
  };

  const handleFormSubmit = async (data) => {
    if (selectedItem) {
      await updateItem(selectedItem.id, data);
    } else {
      await createItem(data);
    }
    fetchData();
    handleCloseModal();
  };
  const [openDelete, setOpenDelete] = useState(false);
  const handleOpenDelete = (task) => {
    setOpenDelete(true);
  };
  const handleCloseDelete = () => {
    setOpenDelete(false);
  };
  const handleDelete = (id) => {
    alert(id);
    fetch(`http://localhost:3000/todoList/${id}`, {
      method: "DELETE",
    })
      .then((res) => {
        console.log(res);
        setOpenDelete(false);
        setItems(items.filter((p) => p.id !== id));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const [expandedList, setExpandedList] = useState([]);
  const handleExpandToggle = (index) => {
    setExpandedList((prevExpandedList) => {
      const newList = [...prevExpandedList];
      newList[index] = !newList[index];
      return newList;
    });
  };

  return (
    <div>
      <Container sx={{ display: "flex", justifyContent: "center" }}>
        <Box
          sx={{
            width: "80%",
            margin: "15px",
          }}
        >
          <Typography
            sx={{ textAlign: "center", fontWeight: "bold" }}
            variant="h6"
          >
            Add your FAQ'S here
          </Typography>
          <ButtonGroup sx={{ display: "flex", justifyContent: "center" }}>
            <Button
              variant="contained"
              size="medium"
              sx={{
                color: "#ffffff",
                background: "#73CFF9",
                "&:hover": {
                  backgroundColor: "#73CFF9",
                },
              }}
              onClick={() => handleOpenModal(null)}
            >
              Add FAQ
            </Button>
          </ButtonGroup>
          {items?.length > 0 &&
            items
              ?.slice(0)
              .reverse()
              ?.map((li, index) => (
                <List key={li?.id} sx={style}>
                  <ListItem sx={{ position: "absolute" }}>
                    {`${li.title}`.charAt(0).toUpperCase() +
                      `${li.title}`.slice(1)}
                  </ListItem>
                  <ButtonGroup
                    sx={{ display: "flex", justifyContent: "end" }}
                    size="small"
                    aria-label="small button group"
                  >
                    <IconButton onClick={() => handleOpenDelete(li)}>
                      <DeleteIcon />
                    </IconButton>
                    <AlertPopUp
                      open={openDelete}
                      onClose={handleCloseDelete}
                      onSubmitClick={() => handleDelete(li?.id)}
                      submitButton="Delete"
                      title={
                        <Typography>
                          Are You sure that you want to delete&nbsp;
                          {selectedItem?.title}?
                        </Typography>
                      }
                    />

                    <IconButton onClick={() => handleOpenModal(li)}>
                      <EditIcon />
                    </IconButton>

                    <IconButton
                      onClick={() => handleExpandToggle(index)}
                      aria-expanded={expandedList}
                      aria-label="show more"
                    >
                      {expandedList[index] ? (
                        <ExpandLessIcon />
                      ) : (
                        <ExpandMoreIcon />
                      )}
                    </IconButton>
                  </ButtonGroup>
                  <Collapse
                    in={expandedList[index]}
                    timeout="auto"
                    unmountOnExit
                  >
                    <ListItem sx={{ color: "grey" }}>
                      {`${li.description}`.charAt(0).toUpperCase() +
                        `${li.description}`.slice(1)}
                    </ListItem>
                  </Collapse>
                </List>
              ))}
          <ModalForm
            open={openModal}
            handleClose={handleCloseModal}
            onSubmit={handleFormSubmit}
            defaultValues={selectedItem}
          />
        </Box>
      </Container>
    </div>
  );
};
export default Home;
