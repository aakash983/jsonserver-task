import React from "react";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import Home from "./Components/Page/Home";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route index path="/" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};
export default App;
